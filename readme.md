# User Management Server

### DB:
Uses postgresql although it can be changed by changing dialect in `./config/dbconfig` to mysql since it uses Sequelize ORM, but it's not tested.
DB config env vars:
```
DB_USERNAME
DB_PASSWORD
DB_NAME
DB_HOSTNAME
```
### Config:
```
npm install
npm run init
npm start
```
if `npm eun init` fails try running:
```
npm run db:create &&  npm run db:migrate
```

### Server:

[http://localhost:8000/](http://localhost:8000/)
