const express = require('express');
const router = express.Router();
const passport = require('passport');

/* POST login or create a new user. */
router.post('/', function (req, res, next) {
  // Default findOrReplace returns error.
  let username = req.body.username;
  if (!username) return res.status(422).send('Username not set');

  req.body.password = 'fakepw'; // hack to make passport-local ignore password

  return passport.authenticate('local-login', (err, token, userData) => {
    if (err || !userData.id) {
      return res.status(400).json({
        success: false,
        message: 'Cannot login'
      });
    }

    return res.json({
      success: true,
      token,
      user: userData
    });
  })(req, res, next);
});

module.exports = router;
