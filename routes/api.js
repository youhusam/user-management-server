const express = require('express');
const router = express.Router();
const User = require('../models').User;

/* GET list users and filter */
router.get('/users', (req, res) => {
  let where = {
    id: {
      $not: req.user.id
    }
  };
  if (req.query.username) {
    where.username = { $like: `${req.query.username}%` };
  }
  if (req.query.status) {
    where.status = req.query.status;
  }
  User.findAll({ where }).then(users => {
    res.status(200).json(users);
  }).catch(() => {
    res.status(400).send('Invalid filters');
  });
});

/* PATCH to update user status */
router.patch('/me', (req, res) => {
  const status = req.body.status;

  if (!status) return res.status(400).send('Must set status');

  return User.update({
    status
  }, {
      where: {
        id: req.user.id
      }
    }).then(() => {
      req.user.status = status;
      return res.status(200).json(req.user);
    }).catch(() => {
      res.status(500).send('Invalid user status');
    });
});

module.exports = router;
