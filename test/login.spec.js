/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "should" }] */
/*
 global describe
 global it
 global before
*/
process.env.NODE_ENV = 'test';

const models = require('../models');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();

chai.use(chaiHttp);

describe('Login', () => {

  before((done) => { //Before each test we empty the database
    models.User.destroy({ where: {} }).then(() => {
      done();
    });
  });

  describe('/POST /login', () => {
    it('it should fail if no user is set on POST', (done) => {
      chai.request(server)
        .post('/login')
        .send({})
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should create a new user and login on POST', (done) => {
      let user = {
        username: "testuser"
      };

      chai.request(server)
        .post('/login')
        .send(user)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('token');
          res.body.should.have.property('user');
          res.body.user.should.have.property('username');
          res.body.user.username.should.be.eql(user.username);
          done();
        });
    });

    it('it should return same user and login on POST', (done) => {
      let user = {
        username: "testuser"
      };

      chai.request(server)
        .post('/login')
        .send(user)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('token');
          res.body.should.have.property('user');
          res.body.user.should.have.property('username');
          res.body.user.username.should.be.eql(user.username);
          done();
        });
    });

  });

});
