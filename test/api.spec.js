/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "should" }] */
/*
 global describe
 global it
 global before
*/
process.env.NODE_ENV = 'test';

const models = require('../models');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();

chai.use(chaiHttp);

describe('API', () => {

  let loggedInUser = {};
  let newUsers = [
    { username: 'foo' },
    { username: 'bar', status: 'On Vacation' },
    { username: 'foobar' },
    { username: 'quux' }
  ];

  before((done) => { // Clear database, create new users, and login.
    models.User.destroy({ where: {} }).then(() => {
      models.User.bulkCreate(newUsers).then(() => {
        chai.request(server)
          .post('/login')
          .send({
            username: 'foo'
          })
          .end((err, res) => {
            loggedInUser = res.body;
            done();
          });
      });
    });
  });

  describe('api/', () => {
    it('it should fail if not logged in', (done) => {
      chai.request(server)
        .get('/api/users')
        .send({})
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
    });

    it('it should return an error if invalid status on PATCH', (done) => {
      chai.request(server)
        .patch('/api/me')
        .set('Authorization', `bearer ${loggedInUser.token}`)
        .send({ status: 'Invalid' })
        .end((err, res) => {
          res.should.have.status(500);
          done();
        });
    });

    it('it should update user\'s status on PATCH', (done) => {
      chai.request(server)
        .patch('/api/me')
        .set('Authorization', `bearer ${loggedInUser.token}`)
        .send({ status: 'On Vacation' })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('object');
          res.body.should.have.property('status');
          res.body.status.should.eql('On Vacation');
          done();
        });
    });

    it('it should return a list of users without current usre if logged in', done => {
      chai.request(server)
        .get('/api/users')
        .set('Authorization', `bearer ${loggedInUser.token}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('array');
          res.body.length.should.be.eql(newUsers.length - 1);
          done();
        });
    });

    it('it should be able to filter users based on username and status', done => {
      chai.request(server)
        .get('/api/users')
        .set('Authorization', `bearer ${loggedInUser.token}`)
        .query({
          username: 'ba',
          status: 'On Vacation'
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('array');
          res.body.length.should.be.eql(1);
          res.body[0].username.should.be.eql('bar');
          res.body[0].status.should.be.eql('On Vacation');
          done();
        });
    });

    it('it should return an error if invalid status in filter', done => {
      chai.request(server)
        .get('/api/users')
        .set('Authorization', `bearer ${loggedInUser.token}`)
        .query({
          username: 'ba',
          status: 'invalid'
        })
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});
