const PassportLocalStrategy = require('passport-local').Strategy;
const User = require('../models').User;
const jwt = require('jsonwebtoken');
const config = require('../config');

/**
 * Return the Passport Local Strategy object.
 */
module.exports = new PassportLocalStrategy({
  session: false,
  passReqToCallback: true
}, (req, username, password, done) => {
  const userData = {
    username: req.body.username.trim()
  };

  return User.findOne({
    where: { username: userData.username }
  }).then(user => {
    if (user) {
      const payload = { sub: user.id };
      const token = jwt.sign(payload, config.jwtSecret);
      return done(null, token, user);
    }
    User.create({ username: userData.username, status: 'Working' })
      .then((user) => {
        const payload = { sub: user.id };
        const token = jwt.sign(payload, config.jwtSecret);
        return done(null, token, user);
      });
  });
});
