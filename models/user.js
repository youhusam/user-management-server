'use strict';
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    username: DataTypes.STRING,
    status: DataTypes.ENUM('Working', 'On Vacation')
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
  return User;
};
