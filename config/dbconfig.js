module.exports = {
  "development": {
    username: process.env.DB_USERNAME || "husam",
    password: process.env.DB_PASSWORD || null,
    database: process.env.DB_NAME || "usermanagement_dev",
    host: process.env.DB_HOSTNAME || "127.0.0.1",
    dialect: "postgres",
  },
  "test": {
    username: process.env.DB_USERNAME || "husam",
    password: process.env.DB_PASSWORD || null,
    database: process.env.DB_NAME || "usermanagement_test",
    host: process.env.DB_HOSTNAME || "127.0.0.1",
    dialect: "postgres",
  },
  "production": {
    username: process.env.DB_USERNAME || "husam",
    password: process.env.DB_PASSWORD || null,
    database: process.env.DB_NAME || "usermanagement_prod",
    host: process.env.DB_HOSTNAME || "127.0.0.1",
    dialect: "postgres"
  }
};
