'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query('ALTER TABLE "Users" ALTER COLUMN status SET NOT NULL, ALTER COLUMN status SET DEFAULT \'Working\';');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query('ALTER TABLE "Users" ALTER COLUMN status DROP NOT NULL, ALTER COLUMN status DROP DEFAULT;');
  }
};
